"use strict";

/* 4 DBG, 3: LOG, 2: WARN, 1: ERR, 0: NONE */
global.native_console = global.console;
global.log_level = 3;
const console = {
    ...global.console,
    error: (...args) => { global.log_level >= 1 && global.native_console.error(...args); },
    warn: (...args) => { global.log_level >= 2 && global.native_console.warn(...args); },
    log: (...args) => { global.log_level >= 3 && global.native_console.log(...args); },
    debug: (...args) => { global.log_level >= 4 && global.native_console.debug(...args); },
    setLevel: (level) => { global.log_level = level; },
    setDebug: (cond) => { if (cond) { global.log_level = 4; } },
};

module.exports = { console };
