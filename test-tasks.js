/*
 * Simple example of using task_thread to run tasks with timeout
 *
 * Execute with:
 * $ node test-workers.js
 *
 * Or in sequential mode with the use of promise:
 * $ env AWAIT=1 ./test-tasks.js
 *
 *
 * Tested on nodejs >= v12.x, does not work on v10.x
 */

"use strict";

const assert = require("assert");
const util = require("util")

// Import from task_thread
const { runTask, postTaskResult, getTaskData, isMain, isTask } = require("./task_thread.js");

// Import configurable console for debugging
const { console } = require("./log_console.js");
console.setDebug(false) // for no output with console.debug (the default)
//console.setDebug(true) // for debugging with console.debug


// Use the same file and dispatch to worker if executed from a Task
// Refer to runTask() below where __filename is passed
if (!isTask) {
    if (process.env.AWAIT) {
        async_main();
    } else {
        main();
    }
} else {
    task_dispatch();
}


/* Code for handling tasks */
function task_dispatch() {
    assert.ok(isTask);
    const functions = {
        "test_long_function": test_long_function,
        "test_bogus_function": test_bogus_function,
        "test_exit_function": test_exit_function,
    };
    const data = getTaskData()
    const funcname = data.funcname;
    const funcargs = data.funcargs;
    const res = functions[funcname](...funcargs);
    postTaskResult(res);
}


/*
  Main program code below: do some tests, expected output is (unordered):
  
  TEST: TEST1: TIMEOUT (2): timeout: after 1000 msecs
  TEST: TEST2: TIMEOUT (2): timeout: after 1000 msecs
  TEST: TEST3: SUCCESS: result: 746450240
  TEST: TEST4: SUCCESS: result: 2336000
  TEST: TEST5: ERROR (1): ReferenceError: test_bogus_function_result is not defined
  TEST: TEST6: ERROR (4): terminated or unknown error
  TEST: TEST7: SUCCESS: result: undefined
  
*/

/* Tests args list */
function getTestsList() {
    const tests = [
        ["TEST1", "test_long_function", [10_000_000_000]],
        ["TEST2", "test_long_function", [10_000_000_000]],
        ["TEST3", "test_long_function", [1_000_000]],
        ["TEST4", "test_long_function", [2_000_000]],
        ["TEST5", "test_bogus_function", []],
        ["TEST6", "test_exit_function", [4]],
        ["TEST7", "test_exit_function", [0]],
    ];
    return tests
}


/*
 * Function from main thread,
 * launch tests in parallel.
 */
function main() {
    assert.ok(isMain);
    const nodeback = (err, res) => {
        if (err) {
            if (err.timeout) {
                console.error(`TEST: ${err.taskname}: TIMEOUT (${err.exit_code}): ${err.error}`);
            } else {
                console.error(`TEST: ${err.taskname}: ERROR (${err.exit_code}): ${err.error}`);
            }
        } else {
            console.log(`TEST: ${res.taskname}: SUCCESS: result: ${JSON.stringify(res.result)}`);
        }
    };
    console.debug("START: main")
    for (const testArgs of getTestsList()) {
        if (testArgs[0] != "TEST2") {
            runTask(...testArgs, __filename, { timeout: 1000}, nodeback);
        } else {
            /* Implement timeout ourselves for test2. */
            var task2 = runTask(...testArgs, __filename, {}, nodeback);
            setTimeout(() => task2.terminate(2, "timeout"), 1000);
        }
    }
    console.debug("END: main")
}


/*
 * Function from main thread, launch tests sequentially.
 * Serialized with promise and await.
 * Simply use util.promisify(runTask) to make it a promise.
 */
async function async_main() {
    assert.ok(isMain);
    const resolved = (res) => {
        console.log(`TEST: ${res.taskname}: SUCCESS: result: ${JSON.stringify(res.result)}`);
    };
    const rejected = (err) => {
        if (err.timeout) {
            console.error(`TEST: ${err.taskname}: TIMEOUT (${err.exit_code}): ${err.error}`);
        } else {
            console.error(`TEST: ${err.taskname}: ERROR (${err.exit_code}): ${err.error}`);
        }
    };
    console.debug("START: async_main")
    const promise = util.promisify(runTask);
    for (const testArgs of getTestsList()) {
        if (testArgs[0] != "TEST2") {
            await promise(...testArgs, __filename, {timeout: 1000}).then(resolved, rejected);
        } else {
            /* Implement timeout ourselves for test2. */
            const promise_timeout = (...args) => {
                return new Promise((resolve, reject) => {
                    const callback = (err, res) => { if (err) { reject(err); } else { resolve(err); } };
                    var task = runTask(...args, callback);
                    setTimeout(() => { task.terminate(2, "timeout") }, 1000);
                })};
            await promise_timeout(...testArgs, __filename, {}).then(resolved, rejected);
        }
    }
    console.debug("END: async_main")
}


/* Exits early with some code */
function test_exit_function(code) {
    process.exit(code);
}


/* Generates a JS error (undefined reference error) */
function test_bogus_function() {
    return test_bogus_function_result; // undefined
}


/* Long running function, much more than 1 sec when iters > 10_000_000_000 */
function test_long_function(iters) {
    console.debug(`START long function: ${iters}`);
    var acc = 0;
    for (let i = 0; i < iters; i++) {
        acc = (1103515245 * acc + 12345) & 0x7fffffff;
    }
    console.debug(`END long function: ${iters} => ${acc}`);
    return acc;
}
