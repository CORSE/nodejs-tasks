NodeJS Tasks
============

A simple example implementation of background and cancellable (with timeout) task.

Refer to example usage in the [test file](test-tasks.js) file.

Refer to implementation a API documentation in:
- `Task` class: [API](doc/task_thread.md), [Sources](task_thread.js)
- `WorkerTimeout` class: [API](doc/worker_timeout.md), [Sources](worker_timeout.js)


Install
-------

No installation necessary, though requirement on NodeJS >= 12.x.


Test
----

Test with:

    node ./test-timeout.js
    TEST: result 1518859008
    TEST: exiting 0
    TEST: exit_code: 0, timeout: false result: 1518859008

    node ./test-tasks.js
    TEST: TEST7: SUCCESS: result: undefined
    TEST: TEST6: ERROR (4): terminated or unknown error
    TEST: TEST3: SUCCESS: result: 746450240
    TEST: TEST4: SUCCESS: result: 2336000
    TEST: TEST5: ERROR (1): ReferenceError: test_bogus_function_result is not defined
    TEST: TEST1: TIMEOUT (2): timeout: after 1000 msecs
    TEST: TEST2: ERROR (2): timeout


Or in sequential mode with the use of promise:

    env AWAIT=1 ./test-tasks.js
    TEST: TEST1: TIMEOUT (2): timeout: after 1000 msecs
    TEST: TEST2: ERROR (2): timeout
    TEST: TEST3: SUCCESS: result: 746450240
    TEST: TEST4: SUCCESS: result: 2336000
    TEST: TEST5: ERROR (1): ReferenceError: test_bogus_function_result is not defined
    TEST: TEST6: ERROR (4): terminated or unknown error
    TEST: TEST7: SUCCESS: result: undefined


Develop
-------

Recreate documentations with:

    npm install -g jsdoc-to-markdown
    jsdoc2md task_thread.js >doc/task_thread.md
    jsdoc2md worker_timeout.js >doc/worker_timeout.md


License
-------

This is distributed unlicensed, refer to The Unlicense: <https://opensource.org/license/unlicense>
