<a name="module_task_thread"></a>

## task\_thread
Implements an interruptible Task and Task utility functions
on top of worker threads with timeout (ref to `WorkerTimeout`).


* [task_thread](#module_task_thread)
    * [~Task](#module_task_thread..Task)
        * [new Task(taskname, funcname, funcargs, filename, [options], [nodeback])](#new_module_task_thread..Task_new)
        * [.run()](#module_task_thread..Task+run)
        * [.terminate([exit_code], [error])](#module_task_thread..Task+terminate)
    * [~isMain](#module_task_thread..isMain)
    * [~isTask](#module_task_thread..isTask)
    * [~runTask()](#module_task_thread..runTask) ⇒ <code>Task</code>
    * [~getTaskData()](#module_task_thread..getTaskData)
    * [~postTaskResult()](#module_task_thread..postTaskResult)

<a name="module_task_thread..Task"></a>

### task_thread~Task
Task is an asynchroneous and interruptible task (termination or timeout).

It calls back the given `nodeback(err, res)` with:

- if `err` is not `null`, an error/terminate/timeout occured and
  `err` contains:
    - `.taskname`: the task name passed in the constructor
    - `.exit_code`: a non 0 exit code, either the exit code from the task itself
      if it caused an error or was exited with `process.exit(code)` with `code`
      not 0, or 1 if it was terminated from the parent with the `terminate()`
      method, or due to the timeout (ref to `.timeout` below)
    - `.error`: the error string
    - `.timeout`: a boolean specifying if the task was terminated due to
      the timeout passed in the constructor
- otherwise, on succesful completion `res` contains:
    - `.taskname`: the task name passed in the constructor
    -  `.result`: the task result as passed fron the task through `postTaskResult(result)`
     or `undefined` if the task does not generate result

Use it for instance as shown below:

    const nodeback = (err, res) => {
      if (err && err.timeout) {
        console.log(`Timeout`);
      } else if (err) {
        console.log(`Error (${err.exit_code}): ${err.error}`);
      } else {
        console.log(`Result:${res.result}`);
      }
    var task = Task("TASK1", "task1", [1, 2], "tasks.js", { timeout: 1000 }, nodeback)
    task.run()

Where for intance in `tasks.js` we have:

    ...
    assert(isTask);
    const data = getTaskData();
    if (data.funcname == "task1") {
      const res = task1(...data.funcargs);
      postTaskResult(res);
    } else {
      throw new Error(`Unexpeted task function name: ${data.funcname}`)
    }

**Kind**: inner class of [<code>task\_thread</code>](#module_task_thread)  

* [~Task](#module_task_thread..Task)
    * [new Task(taskname, funcname, funcargs, filename, [options], [nodeback])](#new_module_task_thread..Task_new)
    * [.run()](#module_task_thread..Task+run)
    * [.terminate([exit_code], [error])](#module_task_thread..Task+terminate)

<a name="new_module_task_thread..Task_new"></a>

#### new Task(taskname, funcname, funcargs, filename, [options], [nodeback])
Task construction, no execution happens at this point.


| Param | Type | Description |
| --- | --- | --- |
| taskname | <code>string</code> | name of the task for identification in the returned response/error |
| funcname | <code>string</code> | name of the function to call,                 it is up to the task filename to interpret this argument |
| funcargs | <code>Array</code> | arguments to pass to the funciton (or `[]` or `undefined`),                it is up to the task filename to interpret this argument |
| filename | <code>string</code> | file to execute for the task (this file must get the `funcname`/`funcargs`                 from the `getTaskData()` object, execute the request and optionally return a result                 with `postTaskResult(result)` |
| [options] | <code>object</code> | passed to the underlying `WorkerTimeout` object,        for instance: `{ timeout: 1000 }` for setting a task termination timeout of 1 sec |
| [nodeback] | <code>callback</code> | the user callback in nodeback style, i.e. called as `nodeback(err, res)`,                              see the class usage above for details |

<a name="module_task_thread..Task+run"></a>

#### task.run()
Run the Task

After calling this functionm the task should be considered running in a backgrounbd thread.
The user callback may be called as soon as the event loop is available.
A running task can be interrupted from the launcher either due to:

- the `timeout` defined in the constructors `options`
- a call to `terminate()` on this object

Otherwise, the task itself may terminate from the executed `filename` either due to:

- a succesful end of the execution
- an error while executing
- an explicit call to `process.exit(code)`

**Kind**: instance method of [<code>Task</code>](#module_task_thread..Task)  
<a name="module_task_thread..Task+terminate"></a>

#### task.terminate([exit_code], [error])
Terminate the task

Terminates a running task with optional exit code and error message.

**Kind**: instance method of [<code>Task</code>](#module_task_thread..Task)  

| Param | Type | Description |
| --- | --- | --- |
| [exit_code] | <code>number</code> | optional exit code to pass to the task `err`, or 1 if undefined.                             When 0 the task will be seen as succesful from the parent |
| [error] | <code>string</code> | optional error string if `exit_code` is not 0 |

<a name="module_task_thread..isMain"></a>

### task_thread~isMain
True only in the main thread (i.e. not in a Task).

**Kind**: inner constant of [<code>task\_thread</code>](#module_task_thread)  
<a name="module_task_thread..isTask"></a>

### task_thread~isTask
True only in a task thread (i.e. not in the main thread).

**Kind**: inner constant of [<code>task\_thread</code>](#module_task_thread)  
<a name="module_task_thread..runTask"></a>

### task_thread~runTask() ⇒ <code>Task</code>
Create a task and call its `run()` method.
This function is actually a shortcut for:

    var task = Task(...taskArgs);
    task.run();

Optionally one may promisify this function and run a task as follow:

    const util = require("util");
    const resolved = (res) =>  { console.log(`Result: ${res.result}`); };
    const rejected = (err) =>  { console.log(`Error: ${err.error}`); };
    const promise = util.promisify(runTask);
    await promise(...taskArgs).then(resolved, rejected);

**Kind**: inner method of [<code>task\_thread</code>](#module_task_thread)  
**Returns**: <code>Task</code> - the created task  
<a name="module_task_thread..getTaskData"></a>

### task_thread~getTaskData()
Gets the task input data which contains at least:

- `.taskname`: the task name as passed to the `Task` constructor
- `.funcname`: the function name as passed to the `Task` constructor
- `.funcargs`: the funciton args as passed to the `Task` constructor

**Kind**: inner method of [<code>task\_thread</code>](#module_task_thread)  
<a name="module_task_thread..postTaskResult"></a>

### task_thread~postTaskResult()
Returns the task result to the parent, can be anything including undefined
if no result is generated. Refer to the class documentation above for details.

**Kind**: inner method of [<code>task\_thread</code>](#module_task_thread)  
