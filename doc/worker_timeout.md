<a name="module_worker_timeout"></a>

## worker\_timeout
Implements timeout functionality on top of `Worker` class.

Ref to Worker doc at <https://nodejs.org/api/worker_threads.html#worker-threads>


* [worker_timeout](#module_worker_timeout)
    * [~WorkerTimeout](#module_worker_timeout..WorkerTimeout)
        * [new WorkerTimeout(filename, [options])](#new_module_worker_timeout..WorkerTimeout_new)
        * [.is_timeout()](#module_worker_timeout..WorkerTimeout+is_timeout) ⇒ <code>boolean</code>
        * [.emit(type, [...args])](#module_worker_timeout..WorkerTimeout+emit)

<a name="module_worker_timeout..WorkerTimeout"></a>

### worker_timeout~WorkerTimeout
WorkerTimeout is a Worker with an optional timeout option.

If timeout is specified and > 0, the worker will be terminated
after the given timeout if still running.

After exit the `.is_timeout()` function returns true if the timeout
was trigerred. For instance:

    var worker = WorkerTimeout(file, {timeout: 1000, ...});
    worker.on("exit", (code) => {
               if (worker.is_timeout()) {
                   console.log(`timeout after 1000 msecs`);
               } else {
                   console.log(`exit code: ${code}`);
               }
    });

Also the "exit" event receives a code 2 if the timeout was trigger
instead of 1 for a bare `.terminate()`. Note that the task
may return itself a code 2, hence, always test the timeout condition
with `.is_timeout()`.

In addition event handlers for `"timeout"` can be registered with
for instance:

    var worker = WorkerTimeout(file, {timeout: 1000, ...});
    worker.on("timeout", (msecs) => {
               console.log(`timeout after ${msecs} msecs`);
    });

The `"timeout"` event is emitted before the `"exit"` event.

**Kind**: inner class of [<code>worker\_timeout</code>](#module_worker_timeout)  

* [~WorkerTimeout](#module_worker_timeout..WorkerTimeout)
    * [new WorkerTimeout(filename, [options])](#new_module_worker_timeout..WorkerTimeout_new)
    * [.is_timeout()](#module_worker_timeout..WorkerTimeout+is_timeout) ⇒ <code>boolean</code>
    * [.emit(type, [...args])](#module_worker_timeout..WorkerTimeout+emit)

<a name="new_module_worker_timeout..WorkerTimeout_new"></a>

#### new WorkerTimeout(filename, [options])
Same constructor as `Worker`


| Param | Type | Description |
| --- | --- | --- |
| filename | <code>string</code> | the file to load and execute |
| [options] | <code>object</code> | passed to the worker, plus an optional `timeout`                 option specified in msecs |

<a name="module_worker_timeout..WorkerTimeout+is_timeout"></a>

#### workerTimeout.is\_timeout() ⇒ <code>boolean</code>
Returns whether the timeout caused the termination.

**Kind**: instance method of [<code>WorkerTimeout</code>](#module_worker_timeout..WorkerTimeout)  
**Returns**: <code>boolean</code> - true if termnated on timeout  
<a name="module_worker_timeout..WorkerTimeout+emit"></a>

#### workerTimeout.emit(type, [...args])
Override emit in order to return code 2 for timeout
instead of 1 for a bare `terminate()`.
Note that one should use `is_timeout()` for testing a timeout
condition as a code 2 may be returned by the worker itself on
a call to `process.exit(2)` from the worker thread.

**Kind**: instance method of [<code>WorkerTimeout</code>](#module_worker_timeout..WorkerTimeout)  

| Param | Type | Description |
| --- | --- | --- |
| type | <code>str</code> | event type |
| [...args] | <code>any</code> | event args |

