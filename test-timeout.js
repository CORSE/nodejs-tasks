/*
 * Test worker timeout
 *
 */

"use strict";

const assert = require("assert");
const { isMainThread, parentPort, workerData } = require("worker_threads");
const { WorkerTimeout } = require("./worker_timeout.js");
const { console } = require("./log_console.js");

if (isMainThread) {
    main();
} else {
    task_dispatch();
}

function main() {
    assert.ok(isMainThread);
    var worker = new WorkerTimeout(
        __filename,
        {
            workerData: 10,
            timeout: 1000
        });
    var result;
    var exit_code;
    worker.on("message", (res) => {
        console.log(`TEST: result ${res}`);
        result = res;
    });
    worker.on("exit", (code) => {
        console.log(`TEST: exiting ${code}`);
        exit_code = code;
    });
    worker.on("timeout", (msecs) => {
        console.log(`TEST: timeout after ${msecs}`);
    });
    // Test that the timeout is not set after exit
    setTimeout(() => {
        console.log(`TEST: exit_code: ${exit_code}, timeout: ${worker.is_timeout()} result: ${result}`);
        assert.ok(!worker.is_timeout());
    }, 2000);
}

function task_dispatch() {
    assert.ok(!isMainThread);
    const functions = {
        "test_long_function": test_long_function,
    };
    const iters = workerData
    const res = test_long_function(iters);
    parentPort.postMessage(res);
}

/* Long running function, much more than 1 sec when iters > 10_000_000_000 */
function test_long_function(iters) {
    console.debug(`START long function: ${iters}`);
    var acc = 0;
    for (let i = 0; i < iters; i++) {
        acc = (1103515245 * acc + 12345) & 0x7fffffff;
    }
    console.debug(`END long function: ${iters} => ${acc}`);
    return acc;
}
