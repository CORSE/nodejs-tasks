/**
 * Implements timeout functionality on top of `Worker` class.
 *
 * Ref to Worker doc at <https://nodejs.org/api/worker_threads.html#worker-threads>
 *
 * @module worker_timeout
 */

"use strict";

const { Worker } = require("worker_threads");


/**
 * WorkerTimeout is a Worker with an optional timeout option.
 *
 * If timeout is specified and > 0, the worker will be terminated
 * after the given timeout if still running.
 *
 * After exit the `.is_timeout()` function returns true if the timeout
 * was trigerred. For instance:
 *
 *     var worker = WorkerTimeout(file, {timeout: 1000, ...});
 *     worker.on("exit", (code) => {
 *                if (worker.is_timeout()) {
 *                    console.log(`timeout after 1000 msecs`);
 *                } else {
 *                    console.log(`exit code: ${code}`);
 *                }
 *     });
 *
 * Also the "exit" event receives a code 2 if the timeout was trigger
 * instead of 1 for a bare `.terminate()`. Note that the task
 * may return itself a code 2, hence, always test the timeout condition
 * with `.is_timeout()`.
 *
 * In addition event handlers for `"timeout"` can be registered with
 * for instance:
 *
 *     var worker = WorkerTimeout(file, {timeout: 1000, ...});
 *     worker.on("timeout", (msecs) => {
 *                console.log(`timeout after ${msecs} msecs`);
 *     });
 *
 * The `"timeout"` event is emitted before the `"exit"` event.
 *
 */
class WorkerTimeout extends Worker {
    /**
     * Same constructor as `Worker`
     *
     * @param {string} filename the file to load and execute
     * @param {object} [options] passed to the worker, plus an optional `timeout`
     *                 option specified in msecs
     */
    constructor(filename, options) {
        super(filename, options);
        this.timeout = false;
        this.timerId = null;
        if (options && options.timeout > 0) {
            super.once("exit", (code) => {
                if (this.timerId != null) {
                    clearTimeout(this.timerId);
                    this.timerId = null;
                }});
            this.timerId = setTimeout(() => {
                this.timeout = true;
                this.emit("timeout", options.timeout);
                this.terminate()
            }, options.timeout);
        }
    }


    /**
     * Returns whether the timeout caused the termination.
     *
     * @return {boolean} true if termnated on timeout
     */
    is_timeout() {
        return this.timeout;
    }
    
    /**
     * Override emit in order to return code 2 for timeout
     * instead of 1 for a bare `terminate()`.
     * Note that one should use `is_timeout()` for testing a timeout
     * condition as a code 2 may be returned by the worker itself on
     * a call to `process.exit(2)` from the worker thread.
     *
     * @param {str} type event type
     * @param {any} [args] event args
     */
    emit(type, ...args) {
        if (type == "exit" && this.timeout) {
            args[0] = 2;
        }
        return super.emit(type, ...args);
    }
}


module.exports = { WorkerTimeout };
